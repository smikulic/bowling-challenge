/*global window,document,console,function*/
/*jslint plusplus: true */
window.onload = function () {
    var btnStartGame = document.getElementById('startGame'),
        btnRoll = document.getElementById('roll'),
        btnFrameStart = document.getElementById('frameStart'),
        frame = document.getElementById('frame'),
        pins = document.getElementById('pins'),
        frameNum = document.getElementById('frameNum'),
        total = document.getElementById('total'),
        strikes = document.getElementById('strikes'),
        lastScoreboardPanel = document.getElementById("f10"),
        btnGreenClass = "btn btn-green",
        strikeNum = 0;

    function randomFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    btnStartGame.onclick = function startGame() {
        var rollCount = 0,
            frameCount = 1,
            knockedDown = 0,
            score = 0,
            tempScore = 0,
            result = 0,
            strike = 0;

        function resetPins() {
            pins.innerHTML = 10;
        }
        function resetCountAndPins() {
            resetPins();
            rollCount = 0;
        }
        function clearFrame(score) {
            var saveResult = document.getElementById("f" + frameCount);
            saveResult.innerHTML = score;
            result = result + score;
            frameCount++;

            if (frameCount >= 11) {
                console.log("=========== End Game =============");
                total.innerHTML = result;
                btnFrameStart.className = "hide";
                lastScoreboardPanel.parentNode.className = "cleared";
            } else {
                console.log("=========== New frame =============");
                total.innerHTML = result;
                frameNum.innerHTML = frameCount;
                btnFrameStart.className = btnGreenClass;
                saveResult.parentNode.className = "cleared";
            }
            btnRoll.className = "hide";
            strikes.innerHTML = strikeNum;
        }
        function scoreStrikeOne() {
            strike = 0;
            score = score + 10;
            clearFrame(score);
        }

        // show game
        btnStartGame.className = btnStartGame.className + " hide";
        frame.className = frame.className - "hide";

        // click on frame
        btnFrameStart.addEventListener('click', function () {
            btnFrameStart.className = "hide";
            btnRoll.className = btnGreenClass;

            console.log("Frame: " + frameCount);
            resetCountAndPins();
        });

        // click on roll
        btnRoll.addEventListener('click', function () {
            rollCount++;
            console.log("Roll: " + rollCount);

            knockedDown = randomFromInterval(0, 10);
            console.log('Knocking down: ' + knockedDown);

            if (strike === 0) {
                console.log("!Normal game!");
                if (rollCount === 1 && knockedDown < pins.innerHTML) { // if first roll and not a strike
                    pins.innerHTML = pins.innerHTML - knockedDown;
                    score = knockedDown;
                } else if (rollCount === 1 && knockedDown >= pins.innerHTML) { // if first roll and a strike
                    strike = 1;
                    strikeNum++;
                    score = 10;
                    resetCountAndPins();
                } else if (rollCount === 2 && (knockedDown >= pins.innerHTML)) {
                    pins.innerHTML = 0;
                    score = 10;
                    clearFrame(score);
                } else if (rollCount === 2 && (knockedDown < pins.innerHTML)) {
                    pins.innerHTML = pins.innerHTML - knockedDown;
                    score = score + knockedDown;
                    clearFrame(score);
                }

            } else if (strike === 1) {
                console.log("!Strike game 1 ! carry score: " + score);
                if (rollCount === 1 && knockedDown < pins.innerHTML) { // if first roll and not a strike
                    pins.innerHTML = pins.innerHTML - knockedDown;
                    tempScore = knockedDown;
                } else if (rollCount === 1 && knockedDown >= pins.innerHTML) { // if first roll and a strike
                    scoreStrikeOne();
                } else if (rollCount === 2 && (knockedDown >= pins.innerHTML)) {
                    scoreStrikeOne();
                } else if (rollCount === 2 && (knockedDown < pins.innerHTML)) {
                    strike = 0;
                    score = score + knockedDown + tempScore;
                    tempScore = 0;
                    clearFrame(score);
                }

            }
        });
    };
}